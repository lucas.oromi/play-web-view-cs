﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mshtml;

namespace DefWpf4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            this.WebBrowserControl.Navigate("https://Google.com");

            WebBrowserControl.LoadCompleted += WebBrowserControl_LoadCompleted;
            WebBrowserControl.Navigate(new Uri("https://www.example.com"));
        }

        private void WebBrowserControl_LoadCompleted(object sender, NavigationEventArgs e)
        {
            if (WebBrowserControl.Document is mshtml.HTMLDocument doc)
            {
                var anchors = doc.getElementsByName("yourClassName");
                foreach (var anchor in anchors)
                {
                    if (anchor is mshtml.IHTMLElement element)
                    {
                        element.onclick = new mshtml.HTMLButtonElementEvents2_onclickEventHandler((pDispEvtObj) =>
                        {
                            var href = element.getAttribute("href");
                            OpenInNewWindow(href.ToString());
                            return false; // This will prevent the default action
                        });
                    }
                }
            }
        }

        private void OpenInNewWindow(string url)
        {
            var newWindow = new Window();
            var newBrowser = new WebBrowser();
            newBrowser.Navigate(new Uri(url));
            newWindow.Content = newBrowser;
            newWindow.Show();
        }
    }
}
